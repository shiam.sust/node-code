// const square = (x) => x*x

// console.log(square(3))

const event = {
    eventName: 'Eid',
    foodList: ['semai','jorda','curry'],
    eidPlan(){
        console.log('I will pray namaz in ' + this.eventName)
        this.foodList.forEach((food) => {
            console.log('I will eat ' + food + ' in ' + this.eventName)
        })
    }
}

event.eidPlan()